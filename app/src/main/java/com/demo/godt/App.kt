package com.demo.godt

import android.app.Application
import com.github.ajalt.timberkt.Timber
import com.demo.godt.di.AppComponent
import com.demo.godt.di.AppModule
import com.demo.godt.di.DaggerAppComponent
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
class App : Application() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        // Dagger
        component = DaggerAppComponent.builder().appModule(AppModule(this)).build()

        // Timber
        if (BuildConfig.DEBUG) {
            Timber.plant(timber.log.Timber.DebugTree())
        }

        // Realm
        Realm.init(applicationContext)
        val configuration = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(configuration)
    }
}