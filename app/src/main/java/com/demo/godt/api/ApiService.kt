package com.demo.godt.api

import com.demo.godt.model.Recipe
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
interface ApiService {

    @GET("getRecipesListDetailed?tags=&size=thumbnail-medium&ratio=1")
    fun getRecipes(@Query("limit") take: Int, @Query("from") skip: Int): Observable<List<Recipe>>
}