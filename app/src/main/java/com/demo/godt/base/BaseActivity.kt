package com.demo.godt.base

import android.support.v7.app.AppCompatActivity
import com.demo.godt.utils.toast

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
abstract class BaseActivity : AppCompatActivity(), MvpView {

    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showMessage(resId: Int) {
        toast(resId)
    }
}