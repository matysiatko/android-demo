package com.demo.godt.base

import java.lang.ref.WeakReference

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
abstract class BasePresenter<V : MvpView> : MvpPresenter<V> {

    private var view: WeakReference<V>? = null

    override fun attachView(view: V) {
        this.view = WeakReference(view)
        onAttachView()
    }

    override fun detachView() {
        view?.clear()
        view = null
        onDetachView()
    }

    protected fun getView(): V? = view?.get()

    protected abstract fun onAttachView()

    protected abstract fun onDetachView()
}