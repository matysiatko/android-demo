package com.demo.godt.base

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
interface MvpPresenter<in V : MvpView> {

    fun attachView(view: V)
    fun detachView()
}