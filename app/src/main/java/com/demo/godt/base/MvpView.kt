package com.demo.godt.base

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
interface MvpView {
    fun showMessage(message: String)
    fun showMessage(resId: Int)
}