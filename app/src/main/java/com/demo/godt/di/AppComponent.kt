package com.demo.godt.di

import com.demo.godt.ui.main.MainActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun inject(mainActivity: MainActivity)
}