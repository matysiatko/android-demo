package com.demo.godt.di

import android.content.Context
import com.demo.godt.api.ApiClient
import com.demo.godt.api.ApiService
import com.demo.godt.repository.Repo
import com.demo.godt.storage.StorageManager
import dagger.Module
import dagger.Provides
import io.realm.Realm
import javax.inject.Singleton

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun providesContext(): Context = context

    @Provides
    @Singleton
    fun providesApiClient(): ApiClient = ApiClient()

    @Provides
    @Singleton
    fun providesApiService(apiClient: ApiClient): ApiService
            = apiClient.retrofit.create(ApiService::class.java)

    @Provides
    fun providesRealmInstance(): Realm
            = Realm.getDefaultInstance()

    @Provides
    fun providesStorageManager(realm: Realm): com.demo.godt.storage.StorageManager
            = StorageManager(realm)

    @Provides
    fun providesRepo(storageManager: StorageManager, apiService: ApiService): Repo
            = Repo(storageManager, apiService)
}