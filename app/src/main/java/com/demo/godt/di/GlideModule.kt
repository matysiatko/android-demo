package com.demo.godt.di

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
@GlideModule
class GlideModule : AppGlideModule()