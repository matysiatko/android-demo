package com.demo.godt.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.RealmClass


/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
@RealmClass
open class Ingredient : RealmModel {

    @SerializedName("elements")
    @Expose
    var elements: RealmList<Element>? = null
}