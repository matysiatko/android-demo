package com.demo.godt.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass


/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
@RealmClass
open class Recipe : RealmModel {

    companion object {
        val TITLE = "title"
        val INGREDIENTS = "ingredients.elements.name"
    }

    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Int = 0
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("ingredients")
    @Expose
    var ingredients: RealmList<Ingredient>? = null
    @SerializedName("images")
    @Expose
    var images: RealmList<Image>? = null
}