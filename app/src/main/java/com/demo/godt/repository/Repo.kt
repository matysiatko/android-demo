package com.demo.godt.repository

import com.demo.godt.api.ApiService
import com.demo.godt.model.Recipe
import com.demo.godt.storage.StorageManager
import com.demo.godt.utils.Constants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.OrderedRealmCollection

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
class Repo(
        private val storageManager: StorageManager,
        private val apiService: ApiService
) {

    fun clear() {
        storageManager.clear()
    }

    // NETWORK
    fun getRecipes(skip: Int = Constants.DEFAULT_SKIP, take: Int = Constants.DEFAULT_TAKE)
            : Observable<List<Recipe>> {
        return apiService.getRecipes(take, skip)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun isDataLoaded(): Boolean {
        return storageManager.isDataLoaded()
    }

    fun getCachedRecipes(): OrderedRealmCollection<Recipe> {
        return storageManager.getRecipes()
    }

    fun saveRecipes(recipes: List<Recipe>) {
        storageManager.saveRecipes(recipes)
    }

    fun getRecipesWithText(query: String): OrderedRealmCollection<Recipe> {
        return storageManager.getRecipesWithText(query)
    }
}