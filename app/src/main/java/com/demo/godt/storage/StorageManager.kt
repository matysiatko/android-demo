package com.demo.godt.storage

import com.demo.godt.model.Recipe
import io.realm.Case
import io.realm.OrderedRealmCollection
import io.realm.Realm

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
class StorageManager(private val realm: Realm) {

    fun clear() {
        if (!realm.isClosed) realm.close()
    }

    fun saveRecipes(data: List<Recipe>) {
        realm.executeTransaction { realm.insertOrUpdate(data) }
    }

    fun isDataLoaded(): Boolean = realm.where(Recipe::class.java).findAll().isNotEmpty()

    fun getRecipes(): OrderedRealmCollection<Recipe> {
        return realm.where(Recipe::class.java).findAllSorted(Recipe.TITLE)
    }

    fun getRecipesWithText(query: String): OrderedRealmCollection<Recipe> {
        return realm.where(Recipe::class.java)
                .beginGroup()
                .contains(Recipe.TITLE, query, Case.INSENSITIVE)
                .or()
                .contains(Recipe.INGREDIENTS, query, Case.INSENSITIVE)
                .endGroup()
                .findAllSorted(Recipe.TITLE)
    }
}