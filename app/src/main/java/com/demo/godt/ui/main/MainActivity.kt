package com.demo.godt.ui.main

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import com.demo.godt.App
import com.demo.godt.R
import com.demo.godt.base.BaseActivity
import com.demo.godt.model.Recipe
import com.demo.godt.utils.gone
import com.demo.godt.utils.invisible
import com.demo.godt.utils.visible
import io.realm.OrderedRealmCollection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView {

    @Inject
    lateinit var presenter: MainPresenter

    private lateinit var adapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.component.inject(this)
        presenter.attachView(this)
        setupRecycler()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val item = menu?.findItem(R.id.action_search)
        val searchView = item?.actionView as SearchView
        searchView.queryHint  = getString(R.string.main_search_hint)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                presenter.onSearch(newText)
                return true
            }
        })

        searchView.setOnQueryTextFocusChangeListener { _, queryTextFocused ->
            if (!queryTextFocused) {
                searchView.isIconified = true
                item.collapseActionView()
                searchView.setQuery("", false)
            }
        }
        return true
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showProgress() {
        main_recycler.invisible()
        main_progress.visible()
    }

    override fun hideProgress() {
        main_progress.gone()
        main_recycler.visible()
    }

    override fun updateRecipes(recipes: OrderedRealmCollection<Recipe>) {
        adapter.updateData(recipes)
    }

    private fun setupRecycler() {
        adapter = MainAdapter(presenter.getRecipesCache())
        with(main_recycler) {
            adapter = this@MainActivity.adapter
            layoutManager = LinearLayoutManager(context)
        }
    }
}
