package com.demo.godt.ui.main

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.View
import android.view.ViewGroup
import com.demo.godt.R
import com.demo.godt.di.GlideApp
import com.demo.godt.model.Recipe
import com.demo.godt.utils.inflate
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_recipe.view.*

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
class MainAdapter(recipes: OrderedRealmCollection<Recipe>)
    : RealmRecyclerViewAdapter<Recipe, MainAdapter.RecipeHolder>(recipes, true) {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecipeHolder {
        return RecipeHolder(parent?.inflate(R.layout.item_recipe))
    }

    override fun onBindViewHolder(holder: RecipeHolder?, position: Int) {
        holder?.setupRecipe(data?.get(position))
    }

    class RecipeHolder(view: View?) : RecyclerView.ViewHolder(view) {

        fun setupRecipe(recipe: Recipe?) = with(itemView) {
            recipe?.let {
                // Title
                it.title?.let { item_recipe_title.text = it }

                // Description
                it.description?.let {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        item_recipe_description.text = Html.fromHtml(it, Html.FROM_HTML_MODE_LEGACY)
                    } else {
                        item_recipe_ingredients.text = Html.fromHtml(it)
                    }
                }

                // Ingredients
                it.ingredients?.let {
                    var ingredients = "${context.getString(R.string.main_ingredients)} "
                    // For each ingredient
                    it.forEach {
                        it.elements?.let {
                            // For each element
                            it.forEach {
                                it.name?.let { ingredients += "$it, " }
                            }
                        }
                    }
                    item_recipe_ingredients.text = ingredients
                }

                // Image
                it.images?.let {
                    if (it.isNotEmpty() && it[0].url != null) {
                        GlideApp.with(itemView.context)
                                .load(it[0].url)
//                                .placeholder(R.mipmap.ic_launcher_round)
                                .into(item_recipe_image)
                    }
                }
            }
        }
    }
}