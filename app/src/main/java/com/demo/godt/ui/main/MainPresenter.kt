package com.demo.godt.ui.main

import com.demo.godt.R
import com.demo.godt.base.BasePresenter
import com.demo.godt.model.Recipe
import com.demo.godt.repository.Repo
import io.reactivex.disposables.CompositeDisposable
import io.realm.OrderedRealmCollection
import javax.inject.Inject

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
class MainPresenter @Inject constructor(private val repo: Repo) : BasePresenter<MainView>() {

    private val cd = CompositeDisposable()

    override fun onAttachView() {
    }

    override fun onDetachView() {
        if (cd.size() != 0) cd.clear()
        repo.clear()
    }

    fun start() {
        // Get data if not cached
        if (!repo.isDataLoaded()) getRecipesFromNetwork()
    }

    fun getRecipesCache(): OrderedRealmCollection<Recipe> {
        return repo.getCachedRecipes()
    }

    private fun getRecipesFromNetwork() {
        getView()?.showProgress()
        cd.add(repo.getRecipes().subscribe(
                { recipes ->
                    repo.saveRecipes(recipes)
                    getView()?.hideProgress()
                },
                { t ->
                    getView()?.hideProgress()
                    getView()?.showMessage(R.string.network_error)
                    t.printStackTrace()
                }
        ))
    }

    fun onSearch(query: String?) {
        if (query != null && query.isNotBlank()) getView()?.updateRecipes(repo.getRecipesWithText(query))
        else getView()?.updateRecipes(repo.getCachedRecipes())
    }
}