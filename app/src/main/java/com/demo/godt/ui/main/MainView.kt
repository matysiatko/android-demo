package com.demo.godt.ui.main

import com.demo.godt.base.MvpView
import com.demo.godt.model.Recipe
import io.realm.OrderedRealmCollection

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */
interface MainView : MvpView {
    fun showProgress()
    fun hideProgress()
    fun updateRecipes(recipesWithText: OrderedRealmCollection<Recipe>)
}