package com.demo.godt.utils

/**
 * Created by Mateusz Ziomek on 10/10/17.
 */

object Constants {
    val DEFAULT_SKIP = 0
    val DEFAULT_TAKE = 50
}